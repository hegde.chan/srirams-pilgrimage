package textureHandling;
import java.awt.image.BufferedImage;

import game.Resources;

public class SpriteSheet  {
	public static BufferedImage sheet;
	public enum INDICES{ SRI, BLOCKS };
	public SpriteSheet(int tileSize){
		sheet = Resources.img_spritesheet;
		for (int y = 0; y < sheet.getHeight(); ++y) {
		    for (int x = 0; x < sheet.getWidth(); ++x) {
		         int argb = sheet.getRGB(x, y);
		         if ((argb & 0x00FFFFFF) == 0x0000FFFF)
		         {
		        	 sheet.setRGB(x, y, 0);
		         }
		    }
		}
	}
	
}
