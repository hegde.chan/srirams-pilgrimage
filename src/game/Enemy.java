package game;


public abstract class Enemy extends Sprite{
	protected int speed;
	protected int hp;
	private int w, h;
	
	public Enemy(int x, int y, int w, int h, int speed, int hp){
		super(x,y,w,h);
		this.speed = speed;
		this.hp = hp;
	}
	
	public Enemy(int x, int y, int w, int h){
		this(x, y, w, h, 0, 0);
	}
	
	protected abstract void run(int target_x, int target_y);

}