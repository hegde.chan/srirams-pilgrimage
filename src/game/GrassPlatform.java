package game;

import java.awt.Graphics;

import textureHandling.SpriteSheet;

public class GrassPlatform extends Platform{
	private static final int GRASS_SPRITE_ROW = 1;
	public GrassPlatform(int x, int y){
		super(x, y);
	}

	@Override
	protected void act(Sprite target, boolean collision) {
		return;
	}

	@Override
	public void draw(Graphics g) {
		g.drawImage(SpriteSheet.sheet.getSubimage(0,64*GRASS_SPRITE_ROW, 64,64), (int)x , (int)y, null);
	}
	

}
