package game;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class Level{
	private BufferedImage bg;
	private int[][] spritemap;
	private static int camera_x = 0; //Measured in tiles, used for scrolling
	private static int camera_y = 0;
	private static ArrayList<Sprite> spritesOnScreen = new ArrayList<>();
	private static final int CAMERA_WIDTH = 20;
	private static final int CAMERA_HEIGHT = 12;
	private static final int TILE_SIZE = 64;
	public Level(BufferedImage bg, int[][] spritemap){
		this.bg = bg;
		this.spritemap = spritemap;
	}

	public BufferedImage getBackground(){
		return this.bg;
	}
	
	public void scanInfoPlane(int[][] levelMap){ //Scan level, spawn enemies and platforms accordingly
		for(int i = camera_x; i < levelMap[0].length && i < camera_x + CAMERA_WIDTH; i++){
			for(int j = camera_y; j < levelMap.length && j < camera_y + CAMERA_HEIGHT; j++){
				Sprite s = null;
				int x = (i - camera_x) * TILE_SIZE, y = (j - camera_x) * TILE_SIZE;
				switch(levelMap[j][i]){
					case 2: s = new GrassPlatform(x, y); break;
					default: continue;
				}
				spritesOnScreen.add(s); 
			}
		}
	}
	
	public void refresh(){
		spritesOnScreen.clear();
	}
	
	public void update(Sriram s){
		// s.xOffset and yOffset should tell you where sriram is relative to the level.
		// measurements are in tiles.
	}
	
	
	public int[][] getSpriteMap(){
		return this.spritemap;
	}
}