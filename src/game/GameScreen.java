package game;

import java.awt.Graphics;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.swing.JPanel;

/**
 * class GameScreen: Handles drawing, screen routines, etc
 * @author chandan
 *
 */
@SuppressWarnings("serial")
public class GameScreen extends JPanel implements KeyListener{
	private BufferedImage background;
	private static ArrayList<Sprite> spriteArray = new ArrayList<Sprite>();
	public ArrayList<Integer> keyQueue = new ArrayList<Integer>();
	public boolean keys[] = new boolean[256];
	public boolean isKeyPressed = false;
	public KeyEvent currentEvent;
	
	public GameScreen(){
		this.setFocusable(true);
		this.addKeyListener(this);
	}
	
	@Override
	public void paintComponent(Graphics g){
		g.drawImage(background, 0, 0, Main.WINDOW_WIDTH,Main.WINDOW_HEIGHT, null);
		for(Sprite sprite : spriteArray) sprite.draw(g);
		spriteArray.clear();
	}
	public static void addToDrawQueue(Sprite e) {
		spriteArray.add(e);
	}
	
	public void changeBackground(BufferedImage bi){ //TODO: background scrolling
		this.background = bi;
	}

	//Key handling methods
	@Override
	public void keyPressed(KeyEvent e) {
		currentEvent = e;
		keys[e.getKeyCode()] = isKeyPressed = true;
	}

	@Override
	public void keyReleased(KeyEvent e) {
		currentEvent = e;
		keys[e.getKeyCode()] = isKeyPressed = false;
	}

	@Override
	public void keyTyped(KeyEvent e) {
		
	}
	
	
}
