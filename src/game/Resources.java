package game;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

public class Resources{
	
	public static int NUM_LEVELS = 1; //TODO: Increment when add more levels

	//TILEMAPS
	public static final int[][] LEVEL1MAP = {
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		{2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
	};

	public static void loadAllResources(){
		
		try {

			img_Background1 = loadImage("./res/Level-1-Background.png");
			img_spritesheet = loadImage("./res/Sri_sheet.png");
			img_menu = loadImage("./res/menu.png");
			
		} 
		catch (IOException e) {
			System.out.println("Could not load one or more resources. Please reinstall.");
			e.printStackTrace();
		}
		
		level_backgrounds.add(img_Background1);
		
	}




	

	public static BufferedImage loadImage(String path) throws IOException{
		try{
			File image = new File(path);
			BufferedImage bi = ImageIO.read(image);
			return bi;
		}
		catch(Exception ex){
			ex.printStackTrace();
			throw new IOException();
		}
	}

	private static BufferedImage img_Background1;
	public static BufferedImage img_spritesheet;
	public static BufferedImage img_menu;
	public static final int[][][] LEVEL_ARRAY = {LEVEL1MAP}; //Level sprite data
	
	public static ArrayList<BufferedImage> level_backgrounds = new ArrayList<BufferedImage>();

}