package game;

import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.swing.JFrame;

import textureHandling.SpriteSheet;

public class Main{
	private static JFrame window;
	private static GameScreen screen;
	
	public enum GameState{
		GAME_OVER, GAME_PAUSED, GAME_PLAYING
	};
	public static final int REFRESH_RATE = 60; //Run at 60 FPS
	public static final long FRAME_TIME = 1000000000 / REFRESH_RATE ; 
	
	private static ArrayList<Sprite> spritesOnScreen = new ArrayList<Sprite>();
	
	private static int camera_x = 0; //Measured in tiles, used for scrolling
	private static int camera_y = 0;
	private static final int CAMERA_WIDTH = 20;
	private static final int CAMERA_HEIGHT = 12;
	private static final int TILE_SIZE = 64;
	public static final int WINDOW_WIDTH = 1280, WINDOW_HEIGHT = 768;
	public static void main(String[] args){
		prepareWindow();
		Resources.loadAllResources();
		new SpriteSheet(64);
		gameLoop();
	}
	//Draw main game window
	public static void prepareWindow(){
		window = new JFrame("Sriram's Pilgrimage");
		window.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		screen = new GameScreen();
		window.setContentPane(screen);
		window.setVisible(true);
		window.setResizable(false);
	}
	
	private static void gameLoop(){
		for(int i = 0; i < Resources.NUM_LEVELS; i++){
			BufferedImage currentBackground = Resources.level_backgrounds.get(i);
			screen.changeBackground(currentBackground); 
			scanInfoPlane(Resources.LEVEL_ARRAY[i]); //Update sprite ArrayList with level spr data
			Sriram sri = new Sriram(0,0); 
			while(true){
				long start = System.nanoTime();
				updatePlayer(sri);
				refreshScreen();
				long elapsed = System.nanoTime() - start;
				sleep(FRAME_TIME - elapsed);
			}
		}
	}
	
	private static void scanInfoPlane(int[][] levelMap){ //Scan level, spawn enemies and platforms accordingly
		for(int i = camera_x; i < levelMap[0].length && i < camera_x + CAMERA_WIDTH; i++){
			for(int j = camera_y; j < levelMap.length && j < camera_y + CAMERA_HEIGHT; j++){
				Sprite s = null;
				int x = (i - camera_x) * TILE_SIZE, y = (j - camera_x) * TILE_SIZE;
				//System.out.println(x);
				switch(levelMap[j][i]){
					case 2: s = new GrassPlatform(x, y); break;
					default: continue;
				}
				
				//s.init(); 
				spritesOnScreen.add(s); 
			}
		}
	}
	
	private static void refreshScreen(){
		for(Sprite s: spritesOnScreen){
			s.redraw();
		}
		screen.repaint();
	}
	
	private static void updatePlayer(Sriram s){
	
		if(screen.currentEvent != null && screen.isKeyPressed){
			switch(screen.currentEvent.getKeyCode()){
			case KeyEvent.VK_RIGHT:
				s.controlRoutine(KeyEvent.VK_RIGHT, true);
				break;
			case KeyEvent.VK_LEFT:
				s.controlRoutine(KeyEvent.VK_LEFT, true);
				break;
			case KeyEvent.VK_CONTROL:
				s.controlRoutine(KeyEvent.VK_CONTROL, true);
			}
		}
		else if (screen.currentEvent != null){
			switch(screen.currentEvent.getKeyCode()){
			case KeyEvent.VK_RIGHT:
				s.controlRoutine(KeyEvent.VK_RIGHT, false);
				break;
			case KeyEvent.VK_LEFT:
				s.controlRoutine(KeyEvent.VK_LEFT, false);
				break;
			case KeyEvent.VK_CONTROL:
				s.controlRoutine(KeyEvent.VK_CONTROL, false);
			}
			
		}
		else s.controlRoutine(0, true);
		
		for(Sprite spr: spritesOnScreen){
			spr.run(s);
		}
		s.redraw();
	}
	
	private static void sleep(long delay){
		long elapsed = 0, start = System.nanoTime();
		while(elapsed < delay / 2){
			elapsed += (System.nanoTime() - start);
			start = System.nanoTime();
		}
		
	}
}