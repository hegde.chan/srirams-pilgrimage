package game;
import java.awt.Graphics;
import java.util.ArrayList;



public abstract class Sprite {
	protected double x, y;
	protected int xOffset, yOffset;
	protected int w,h;
	protected double vx, vy; //Velocities for both axes
	protected double ax, ay; //Accelerations for both axes
	protected BoundingBox aabb;
	protected boolean direction = true;
	public boolean collisionDetected = false;

	
	private static final double GRAVITY_ACCEL = 0.4; //Acceleration due to gravity = 5 pixels/frame^2

	ArrayList<BoundingBox> boxes = new ArrayList<>();
	
	public Sprite(double x, double y, int w, int h, int vx, int vy){
		this.x = x;
		this.y = y;
		xOffset = (int) this.x / 64;
		yOffset = (int) this.y / 64;
		this.w = w;
		this.h = h;
		this.aabb = new BoundingBox(this);
		this.vx = vx; this.vy = vy;
		this.ax = 0; this.ay = GRAVITY_ACCEL;
	}

	public Sprite(int x, int y, int w, int h) {
		this(x,y,w,h,0,0);
	}

	public void move(){
		if(y >= 671){
			System.exit(0);
		}
		if( (x < 1 && vx < 0) || (x >=1210 && vx > 0) ) x-=vx;
		x += vx;
		y += vy;
		vy += ay;
		vx += ax;
		aabb.moveTo(this.x, this.y);
	}

	public int getHeight(){
		return this.h;
	}
	
	public int getWidth(){
		return this.w;
	}
	public double getX(){return x;}
	public double getY(){return y;}
	public void stopVMovement(){
		ay = vy = 0;
	}
	
	public void unstop(){
		ay = GRAVITY_ACCEL;
	}
	
	public void redraw(){
		GameScreen.addToDrawQueue(this);
	}
	
	public abstract void draw(Graphics g);

	
	protected abstract void run(Sprite target); //Used by enemies and platforms
	
	
}
