package game;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;

import java.util.ArrayList;

import textureHandling.SpriteSheet;

public class Sriram extends Sprite{
	private static final int SRI_SPRITE_ROW = 0;
	private final static int WALKING_SPEED = 6; 
	private static final int SRI_WALK_POS = 1;
	private static final int TILE_SIZE = 64;
	private static final int NUM_FRAMES = 5;
	private static final int JUMP_SPEED = -11;
	private enum AnimationStates {STAND, WALK, JUMP};
	private AnimationStates animationState = AnimationStates.STAND;
	private double currentFrame = 0.;
	
	
	private ArrayList<BufferedImage> frames;
	
	public Sriram(int x, int y){
		super( x, y, TILE_SIZE, TILE_SIZE, 0, 0);
		frames = new ArrayList<BufferedImage>();
		//go through the sprite sheet and extract the individual frames
		for(int i = 0; i < NUM_FRAMES * TILE_SIZE; i+=TILE_SIZE)
			frames.add(SpriteSheet.sheet.getSubimage(i, SRI_SPRITE_ROW, TILE_SIZE, TILE_SIZE));
		
	}

	public void controlRoutine(int key, boolean isPressed){
		switch(key){
		
		case KeyEvent.VK_LEFT:
			vx = (isPressed)? -WALKING_SPEED:0; 
			break;
		case KeyEvent.VK_RIGHT:
			vx = (isPressed)?WALKING_SPEED:0; 
			break;
		case KeyEvent.VK_CONTROL:
			if(isPressed && vy == 0) vy = JUMP_SPEED;
			unstop();
			break;
		default:
			break;
		}
		if(vx == 0 && vy == 0) animationState = AnimationStates.STAND;
		else if (vy != 0) animationState = AnimationStates.JUMP;
		else animationState = AnimationStates.WALK;
		move();
	}

	@Override
	protected void run(Sprite target) { 
		collisionDetected = aabb.overlapsWith(target.aabb);
	}
	@Override
	public void draw(Graphics g) {
		switch(animationState){
			case WALK:
				direction =  vx > 0;
				currentFrame += 0.25;
				if(currentFrame >= NUM_FRAMES) currentFrame = SRI_WALK_POS;
				break;
			case STAND:
				currentFrame = 0;
				break;
			case JUMP:
				currentFrame = 1;
				break;
		}

		 
		if(!direction){
			//flip the sprite if facing left
			g.drawImage( frames.get((int)currentFrame), (int)this.x + TILE_SIZE, (int)this.y, -TILE_SIZE, TILE_SIZE, null);
		}
		else g.drawImage(frames.get((int)currentFrame), (int)this.x, (int)this.y, null);
		
	}
	
	
	
}
