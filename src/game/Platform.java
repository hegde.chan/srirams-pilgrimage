package game;


public abstract class Platform extends Sprite{
	public Platform(int x, int y){
		super(x, y,64,64);
	}
	
	public void run(Sprite target){
		
	
		if(target.collisionDetected){
			target.run(this);
		}
		else if(target.aabb.overlapsWith(this.aabb)){
			target.collisionDetected = true;
			target.stopVMovement();
			target.y += this.aabb.getYOverlap(target.aabb);
			target.aabb.moveTo(target.x, target.y);
		}
		else{
			target.unstop();
		}
	}
	
	protected abstract void act(Sprite target, boolean collision);
}
